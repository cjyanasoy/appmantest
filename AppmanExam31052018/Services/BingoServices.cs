﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppmanExam31052018.Services
{
    public class BingoServices
    {
        public static DataTable GenerateRowCol(int col,List<int> rows)
        {
            var result = new DataTable();
            for (int i = 0; i < col; i++)
            {
                result.Columns.Add("Col"+(i+1));
            }
            for (int i = 0; i < rows.Count / col; i++)
            {
                DataRow dr = result.NewRow();
                for (var ii = 0; ii < rows.Count / col; ii++)
                {
                    var colName = "Col" + (ii + 1);
                    dr[colName] = rows[ii];
                }
                result.Rows.Add(dr);
            }
            return result;
        }
        public static bool ISBingo(DataTable dt,int col,List<int> input)
        {
            var countrow = dt.Rows.Count;
            var bingo = new List<int>();

            for (var i = 0; i < countrow; i++)
            {
                for (var ii = 0; ii <= col; i++)
                {
                    var o = dt.Rows[ii][i];
                    bingo.Add(Convert.ToInt32(o));
                }
                if (input.Exists(w => bingo.Contains(w)))
                {
                    return true;
                }
            }
            
            return false;
        }
    }
}
