﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppmanExam31052018.Services;
namespace AppmanExam31052018
{
    class Program
    {
        static void Main(string[] args)
        {
            var col = 5;
            var rows = new List<int>();
            for (var i = 1; i <= 25; i++)
            {
                rows.Add(i);
            }
            var input = new List<int> { 3, 4, 8, 13, 18, 19, 23 };
            var bingomap = BingoServices.GenerateRowCol(col, rows);
            BingoServices.ISBingo(bingomap, col, input);
        }
    }
}
